Java Web : Spring Boot + Vue
======
`搭建一個前後端分離的網站架構`

Gitlab 專案
------
+ JavaWeb-MyBatis
+ JavaWeb-Hibernate
+ JavaWeb-JdbcTemplate

### 工具專案內容 : 
1. SpringBoot + MyBatis (ORM) + Thymeleaf (Vue)
2. SpringBoot + Hibernate (ORM) + Thymeleaf (Vue)
3. SpringBoot + JdbcTemplate (ORM) + Thymeleaf (Vue)


JavaWeb-MyBatis 專案進度
------
### 前端功能
1. Thymeleaf + jQuery 
2. Spring Boot + Vue 整合
3. Spring Boot + Vue 前後端

### 後端功能
1. 更換 Tomcat 變成 Jetty V
2. Unit Test (JUnit) V 
3. Package Name V
4. MyBatis (ORM) V
5. SpringBootTest V
6. RestController - RESTful API V
7. Logger (日誌) V
8. 熱部署 V
9. Thymeleaf

### 研究項目
+ Form 表單提交，Controller 接收單獨的參數













參考資料
------
### SpringBoot 跨域訪問
<https://www.gushiciku.cn/pl/ggMi/zh-tw>







