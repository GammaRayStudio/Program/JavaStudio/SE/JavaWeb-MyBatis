package com.enoxs.JavaWebMyBatis;

import com.enoxs.JavaWebMyBatis.infra.repository.gen.dao.AppInfoDAO;
import com.enoxs.JavaWebMyBatis.infra.repository.gen.model.AppInfoPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class AppMainTests {


	@Autowired
	private AppInfoDAO appInfoDAO;

	@Test
	public void runFast() {
		List<AppInfoPO> lstData = appInfoDAO.selectAll();
		System.out.println(lstData.size());

		for (AppInfoPO data : lstData) {
			System.out.println(data.getName());
		}

	}

}
