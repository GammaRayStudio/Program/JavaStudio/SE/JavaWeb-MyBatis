package com.enoxs.JavaWebMyBatis.application;



import com.enoxs.JavaWebMyBatis.domain.app.repo.entity.AppInfo;
import com.enoxs.JavaWebMyBatis.infra.repository.gen.dao.AppInfoDAO;
import com.enoxs.JavaWebMyBatis.infra.repository.gen.model.AppInfoPO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
public class AppInfoAppServiceTest {

    @Autowired
    private AppInfoAppService appInfoAppService;

    @Test
    public void selectAllTest(){
        List<AppInfo> lstData = appInfoAppService.selectAll();

        System.out.println("lstData.size() => " + lstData.size());

        for (AppInfo appInfo : lstData) {
            System.out.println("Id => " + appInfo.getId());
            System.out.println("Name => " + appInfo.getName());
            System.out.println("--- --- --- \n");
        }

    }


}