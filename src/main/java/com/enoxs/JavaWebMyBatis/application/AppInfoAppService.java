package com.enoxs.JavaWebMyBatis.application;


import com.enoxs.JavaWebMyBatis.domain.app.repo.entity.AppInfo;
import com.enoxs.JavaWebMyBatis.domain.app.service.AppInfoService;
import com.enoxs.JavaWebMyBatis.infra.repository.gen.dao.AppInfoDAO;
import com.enoxs.JavaWebMyBatis.infra.repository.gen.model.AppInfoPO;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class AppInfoAppService {

    @Autowired
    private AppInfoService appInfoService;

    @Autowired
    private AppInfoDAO appInfoDAO;
    // Enoxs ADD : 應用層的服務


    public List<AppInfo> selectAll(){
        List<AppInfo> lstData = new LinkedList<>();

        List<AppInfoPO> lstRecord = appInfoDAO.selectAll();
        for (int i = 0; i < lstRecord.size(); i++) {

            AppInfo appInfo = new AppInfo();

            appInfo.setId(lstRecord.get(i).getId());
            appInfo.setName(lstRecord.get(i).getName());
            appInfo.setVersion(lstRecord.get(i).getVersion());
            appInfo.setAuthor(lstRecord.get(i).getAuthor());
            appInfo.setRelease(lstRecord.get(i).getDate() + ""); // Date 型態
            appInfo.setDescription(lstRecord.get(i).getRemark());

            lstData.add(appInfo);
        }
        return lstData;
    }



/*
    int deleteByPrimaryKey(Integer id);

    int insert(AppInfoPO record);

    AppInfoPO selectByPrimaryKey(Integer id);

    List<AppInfoPO> selectAll();

    int updateByPrimaryKey(AppInfoPO record);

    @Select("select * from app_info")
    List<AppInfoPO>selectAppInfo();
*/

}
