package com.enoxs.JavaWebMyBatis.application;

import com.enoxs.JavaWebMyBatis.application.dto.UserInfoDTO;
import com.enoxs.JavaWebMyBatis.controller.AppInfoController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.LinkedList;
import java.util.List;

@Service
public class UserInfoService {
    private static final Logger log = LoggerFactory.getLogger(UserInfoService.class);

    public List<UserInfoDTO> queryUserInfo(){
        List <UserInfoDTO> lstData = new LinkedList<>();
        UserInfoDTO user01 = new UserInfoDTO();
        user01.setAccount("DevAuth01");
        user01.setName("DevAuth01");
        user01.setEmail("DevAuth01@dev.com");

        UserInfoDTO user02 = new UserInfoDTO();
        user02.setAccount("DevAuth02");
        user02.setName("DevAuth02");
        user02.setEmail("DevAuth02@dev.com");

        UserInfoDTO user03 = new UserInfoDTO();
        user03.setAccount("DevAuth03");
        user03.setName("DevAuth03");
        user03.setEmail("DevAuth03@dev.com");

        lstData.add(user01);
        lstData.add(user02);
        lstData.add(user03);
        return lstData;
    }


    public UserInfoDTO queryUserInfoById(Integer id){
        UserInfoDTO userInfo = new UserInfoDTO();
        userInfo.setAccount("DevAuth");
        userInfo.setName("DevAuth");
        userInfo.setEmail("DevAuth@dev.com");
        return userInfo;
    }


    public UserInfoDTO createUserInfo(UserInfoDTO userInfoDTO){
        // 經過 DAO 物件處理
        userInfoDTO.setId(100);

        log.info(userInfoDTO.toString());
        return userInfoDTO;
    }

    public void updateUserInfo(UserInfoDTO userInfoDTO){
        log.info(userInfoDTO.toString());
    }

    public void modifyUserInfo(UserInfoDTO userInfoDTO){
        UserInfoDTO userInfo = new UserInfoDTO();
        userInfo.setAccount("DevAuth");
        userInfo.setName("DevAuth");
        userInfo.setEmail("DevAuth@dev.com");

        if(userInfoDTO.getAccount() != null && !"".equals(userInfoDTO.getName())){
            userInfo.setAccount(userInfoDTO.getAccount());
        }

        if(userInfoDTO.getName() != null && !"".equals(userInfoDTO.getName())){
            userInfo.setName(userInfoDTO.getName());
        }

        if(userInfoDTO.getEmail() != null && !"".equals(userInfoDTO.getEmail())){
            userInfo.setEmail(userInfoDTO.getEmail());
        }

        log.info(userInfo.toString());
    }

    public void deleteUserInfo(Integer id){
        log.info("delete id => " + id);
    }


}
