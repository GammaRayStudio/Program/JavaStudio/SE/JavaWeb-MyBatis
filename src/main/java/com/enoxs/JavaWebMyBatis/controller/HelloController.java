package com.enoxs.JavaWebMyBatis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    private static final Logger log = LoggerFactory.getLogger(AppInfoController.class);

    @RequestMapping("/hello")
    public String hello() {
        log.info("hello.");
        return "Hello World ~ !!!";
    }
}

