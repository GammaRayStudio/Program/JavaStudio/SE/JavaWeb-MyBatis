package com.enoxs.JavaWebMyBatis.controller;

import com.enoxs.JavaWebMyBatis.application.AppInfoAppService;
import com.enoxs.JavaWebMyBatis.application.dto.UserInfoDTO;
import com.enoxs.JavaWebMyBatis.application.UserInfoService;
import com.enoxs.JavaWebMyBatis.domain.app.repo.entity.AppInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * RESTful 風格:
 *
 *    動作   |   描述   |   API
 *   ------ | -------- | --------
 *    GET   | 使用者列表 | /users
 *    GET   | 查詢使用者 | /users/:id
 *    POST	| 新增使用者 | /users
 *    PUT   | 更改使用者 | /users/;id
 *   PATCH  | 更改使用者 | /users/:id
 *   DELETE | 刪除使用者 | /users/:id
 *
 * + PUT : 直接替換
 * + PATCH : 替換部分
 *
 */


@RestController
@RequestMapping("AppInfo/")
public class AppInfoController {
    private static final Logger log = LoggerFactory.getLogger(AppInfoController.class);

    @Autowired
    private AppInfoAppService appInfoAppService;

    @Autowired
    private UserInfoService userInfoService;


    /**
     * 原始方法 : 測試 DB 連線是否正常
     * API : http://localhost:8080/AppInfo/selectAll
     */
    @RequestMapping("selectAll")
    public String selectAll(){
        log.info("selectAll().");
        StringBuffer sbMsg = new StringBuffer(128);
        List<AppInfo> lstData = appInfoAppService.selectAll();

        sbMsg.append("lstData.size() => " + lstData.size() + "<br>");

        for (AppInfo appInfo : lstData) {
            sbMsg.append("Id => " + appInfo.getId() + "<br>");
            sbMsg.append("Name => " + appInfo.getName() + "<br>");
            sbMsg.append("--- --- --- <br>");
        }

        return sbMsg.toString();
    }

    /**
     * 動作 : GET
     * 描述 : 使用者列表
     * API : /users
     */
    @GetMapping("/users")
    public List<UserInfoDTO> queryUserInfo(){
        return userInfoService.queryUserInfo();
    }


    /**
     * 動作 : GET
     * 描述 : 查詢使用者
     * API : /users/:id
     */
    @GetMapping("/users/{id}")
    public UserInfoDTO queryUserInfoById(@PathVariable("id") Integer id){
        log.info("id => " + id);
        return userInfoService.queryUserInfoById(id);
    }


    /**
     * 動作 : POST
     * 描述 : 新增使用者
     * API : /users
     */
    @PostMapping("/users")
    public UserInfoDTO createUserInfo(@RequestBody UserInfoDTO userInfoDTO){
        return userInfoService.createUserInfo(userInfoDTO);
    }


    /**
     * 動作 : PUT
     * 描述 : 更改使用者 (全部)
     * API : /users/:id
     */
    @PutMapping("/users/{id}")
    public void updateUserInfo(@RequestBody UserInfoDTO userInfoDTO){
        userInfoService.updateUserInfo(userInfoDTO);
    }

    /**
     * 動作 : PATCH
     * 描述 : 更改使用者 (部分)
     * API : /users/:id
     */

    @PostMapping("/users/{id}")
    public void modifyUserInfo(@RequestBody UserInfoDTO userInfoDTO){
        userInfoService.modifyUserInfo(userInfoDTO);
    }


    /**
     * 動作 : DELETE
     * 描述 : 刪除使用者
     * API : /users/:id
     */
    @DeleteMapping("/users/{id}")
    public void deleteUserInfo(@PathVariable Integer id){
        userInfoService.deleteUserInfo(id);
    }

}
