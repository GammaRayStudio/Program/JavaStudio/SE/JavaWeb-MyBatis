package com.enoxs.JavaWebMyBatis.domain.app.service;

import com.enoxs.JavaWebMyBatis.domain.app.repo.entity.AppInfo;

import java.util.List;

public interface AppInfoService {
    AppInfo getAppInfo();
    List<AppInfo> findAll();
    AppInfo findById(Long id);
    AppInfo create(AppInfo appInfo);
    void update(AppInfo appInfo);
    void delete(Long id);
}
