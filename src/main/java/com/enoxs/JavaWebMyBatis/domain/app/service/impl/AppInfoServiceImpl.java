package com.enoxs.JavaWebMyBatis.domain.app.service.impl;

import com.enoxs.JavaWebMyBatis.domain.app.repo.entity.AppInfo;
import com.enoxs.JavaWebMyBatis.domain.app.service.AppInfoService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppInfoServiceImpl implements AppInfoService {


    @Override
    public AppInfo getAppInfo() {
        return null;
    }

    @Override
    public List<AppInfo> findAll() {
        return null;
    }

    @Override
    public AppInfo findById(Long id) {
        return null;
    }

    @Override
    public AppInfo create(AppInfo appInfo) {
        return null;
    }

    @Override
    public void update(AppInfo appInfo) {

    }

    @Override
    public void delete(Long id) {

    }
}
