package com.enoxs.JavaWebMyBatis.infra.repository.gen.model;

import java.io.Serializable;
import java.util.Date;

public class AppInfoPO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String name;
    private String version;
    private String author;
    private Date date;
    private String remark;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
