package com.enoxs.JavaWebMyBatis.infra.repository.gen.dao;

import com.enoxs.JavaWebMyBatis.infra.repository.gen.model.AppInfoPO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


@Mapper
public interface AppInfoDAO {
    int deleteByPrimaryKey(Integer id);

    int insert(AppInfoPO record);

    AppInfoPO selectByPrimaryKey(Integer id);

    List<AppInfoPO> selectAll();

    int updateByPrimaryKey(AppInfoPO record);

    @Select("select * from app_info")
    List<AppInfoPO>selectAppInfo();
}
